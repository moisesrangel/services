"""
    __author__     = "Moisés Rangel"
    __license__    = "Private"
    __version__    = "0.1"
    __maintainer__ = "Moisés Rangel"
    __email__      = "moises.rangel@gmail.com"
    __status__     = "Beta"

"""

from core.models                import Branch, Doctor, Service, SpecialCommission, Order, Profile as CustomProfile, Promo, Code
from django                     import forms
from django.contrib             import admin
from django.contrib.auth.admin  import UserAdmin
from django.contrib.auth.models import User
from django.db                  import models
from django.utils.translation   import gettext_lazy as _
from import_export              import resources
from import_export.admin        import ImportExportModelAdmin
from rangefilter.filter         import DateRangeFilter, DateTimeRangeFilter
from django.shortcuts           import render


class ServiceResource(resources.ModelResource):
    class Meta:
        model = Service

class OrderResource(resources.ModelResource):
    class Meta:
        model = Order

class ServiceAdmin(ImportExportModelAdmin):
    resource_class = ServiceResource
    #formfield_overrides = {
    #    models.ManyToManyField: {'widget': forms.CheckboxSelectMultiple},
    #}

class BranchAdmin(admin.ModelAdmin):
    fields = ( 'imagen_logo', 'logo', 'name', 'address', 'phone' )
    readonly_fields = ('imagen_logo',)

class SpecialCommissionAdmin(admin.ModelAdmin):
    list_filter = ['doctor', 'service']


class SucursalProfileListFilter(admin.SimpleListFilter):
    title          = _('Sucursal')
    parameter_name = 'branch'

    def lookups(self, request, model_admin):
        return (
            ('lorem', _('lorem')),
            ('ipsum', _('ipsum')),
        )

    def queryset(self, request, queryset):
        if request.user.is_superuser:
            return queryset

        branches = request.user.profile.branches.all()
        for branch in branches:
            queryset = queryset.filter(branch=branch)

        return queryset

class OrderAdmin(admin.ModelAdmin):
    #change_list_template = 'core/change_list_template_reports.html'
    #resource_class  = OrderResource
    list_display    = ['id', 'name', 'service', 'branch', 'doctor','created_date']
    list_filter     = [('created_date', DateRangeFilter), 'doctor', 'service', 'branch', SucursalProfileListFilter]
    readonly_fields = ['branch', 'commissionquantity', 'commission_dentist', 'commission_doctor', 'doctor', 'hprice', 'total', 'promo', 'code', 'quantity', 'service', 'patient', 'age', 'discount', 'discountquantity']
    search_fields   = ['pk']
    actions         = ['generate_report']

    class Media:
        js = (
            'core/admin/js/orders.js',   # app static folder
        )

    def has_add_permission(self, request, obj=None):
        return False

    def generate_report(self, request, queryset):

        total_services    = [(lambda x: x.quantity)(x) for x in queryset]
        total_discount    = [(lambda x: x.discountquantity)(x) for x in queryset]
        total_commissions = [(lambda x: x.commissionquantity)(x) for x in queryset]
        total_sales       = [(lambda x: x.total)(x) for x in queryset]

        return render(request,
                              'admin/report.html',
                              context=
                              {
                                'data' : queryset,
                                'total_services' : sum(total_services),
                                'total_discount' : sum(total_discount),
                                'total_commissions' : sum(total_commissions),
                                'total_sales' : sum(total_sales),
                              })

        #queryset.update(status='NEW_STATUS')

    generate_report.short_description = "Generar Reporte"

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

class ProfileInline(admin.StackedInline):
    can_delete = False
    fk_name    = 'user'
    model      = CustomProfile
    verbose_name_plural = 'Perfil'

class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline, )

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


admin.site.site_header = 'Laboratorio App'
admin.site.index_title = 'Administración de Catálogos'

admin.site.register(Branch, BranchAdmin)
admin.site.register(Doctor)
admin.site.register(Service, ServiceAdmin)
admin.site.register(SpecialCommission, SpecialCommissionAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Promo)
admin.site.register(Code)
#admin.site.register(CustomProfile)

admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)

