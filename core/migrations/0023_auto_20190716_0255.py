# Generated by Django 2.2.3 on 2019-07-16 02:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_auto_20190716_0244'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='commission',
            new_name='commissionquantity',
        ),
    ]
