"""
    __author__     = "Moisés Rangel"
    __license__    = "Private"
    __version__    = "0.1"
    __maintainer__ = "Moisés Rangel"
    __email__      = "moises.rangel@gmail.com"
    __status__     = "Beta"

"""


from django.conf                     import settings
from django.contrib.auth.models      import User
from django.db                       import models
from django.db.models.signals        import pre_save, post_save
from django.dispatch                 import receiver
from django.template.defaultfilters  import slugify
from django.utils.safestring         import mark_safe

import hashlib
import os

class BaseModel(models.Model):
    created_date  = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Branch(BaseModel):
    logo    = models.ImageField(upload_to = 'images/', default = 'images/logo-default.jpg')
    name    = models.CharField(max_length=200, verbose_name='Nombre', help_text='Nombre de la sucursal.')
    address = models.TextField(verbose_name='Dirección', help_text='Dirección física de la sucursal.')
    phone   = models.CharField(max_length=200, verbose_name='Teléfono', help_text='Teléfono de contacto.')

    #https://teamtreehouse.com/community/django-display-imagefield-with-adminsiteregistermodelmymodel-trick
    def url(self):
        return os.path.join(settings.MEDIA_URL, 'images', os.path.basename(str(self.logo)))

    def imagen_logo(self):
        return mark_safe('<img src="{}" width="150" height="150" />'.format(self.url()) )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'sucursal'
        verbose_name_plural  = 'sucursales'

class Profile(models.Model):
    user     = models.OneToOneField(User, on_delete=models.CASCADE)
    branches = models.ManyToManyField(Branch, blank=True, verbose_name='Sucursales')

    def __str__(self):
        return self.user.username

class Doctor(BaseModel):

    TYPE_CHOICES = (
        ('medico_general', 'Médico General'),
        ('dentista', 'Dentista'),
        )

    name        = models.CharField(max_length=100, verbose_name='Nombre')
    lastname    = models.CharField(max_length=100, verbose_name='Apellido Materno')
    middlename  = models.CharField(max_length=100, verbose_name='Apellido Paterno')
    email       = models.EmailField(max_length=100, verbose_name='Email', help_text='Email del doctor.')
    hospital    = models.CharField(max_length=100, verbose_name='Hospital', help_text='Nombre del hospital al cual pertenece el doctor.')
    address     = models.TextField(verbose_name='Dirección', help_text='Dirección del doctor.')
    phone       = models.CharField(max_length=100, verbose_name='Teléfono', help_text='Télefono del doctor.')
    type_doc    = models.CharField(max_length=100, verbose_name='Tipo de Médico', help_text='Seleccione el tipo de médico', default='medico_general', choices=TYPE_CHOICES)
    document    = models.CharField(max_length=100, verbose_name='Cédula')

    def __str__(self):
        return '{0}, {1}, {2}'.format(self.middlename, self.lastname, self.name)

    class Meta:
        verbose_name = 'doctor'
        verbose_name_plural  = 'doctores'

class Service(BaseModel):
    branches    = models.ManyToManyField(Branch, blank=True, verbose_name='Sucursales')
    code        = models.CharField(max_length=20, verbose_name='Código', unique=True, help_text='Código interno del servicio, debe ser único.')
    description = models.TextField(verbose_name='Descripción', help_text='Nombre descriptivo del servicio.')
    price       = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Precio', help_text='Precio vigente al público.')

    commission_dentist  = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Cantidad de Comisión a Doctor', default=0, help_text='CANTIDAD de comisión para los dentistas.')
    commission_doctor   = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Porcentaje de Comisión a Doctor', default=0, help_text='PORCENTAJE de comisión para los doctores.')

    def __str__(self):
        return '{0}, {1}'.format(self.code, self.description)

    class Meta:
        verbose_name = 'servicio'
        verbose_name_plural  = 'servicios'

class SpecialCommission(BaseModel):
    doctor      = models.ForeignKey(Doctor, on_delete=models.CASCADE, verbose_name='Doctor', help_text='Seleccione el doctor para asignar una comisión especial.')
    service     = models.ForeignKey(Service, on_delete=models.CASCADE, verbose_name='Servicio', help_text='Seleccione un servicio.')
    commission  = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Comisión a Doctor', default=0, help_text='Porcentaje de comisión para los doctores.')

    def __str__(self):
        return '{0}% - {1} - {2}'.format(self.commission, self.service, self.doctor)

    class Meta:
        verbose_name = 'Comisión Especial'
        verbose_name_plural  = 'Comisiones Especiales'

class Promo(BaseModel):
    name        = models.CharField(max_length=20, verbose_name='Nombre promocional', unique=True, help_text='Nombre interno de la promoción, debe ser único.')
    discount    = models.IntegerField(verbose_name='Porcentaje de descuento', help_text='Porcentaje de descuento.')
    init_date   = models.DateTimeField(auto_now_add=False, verbose_name='Fecha de inicio', help_text='Fecha de inicio de la promoción')
    end_date    = models.DateTimeField(auto_now_add=False, verbose_name='Fecha de fin', help_text='Fecha para desactivar la promoción')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Promoción'
        verbose_name_plural ='Promociones'

class Code(BaseModel):
    name        = models.CharField(max_length=20, verbose_name='Nombre promocional', unique=True, help_text='Nombre interno de la promoción, debe ser único.')
    code        = models.CharField(max_length=20, verbose_name='Código promocional', unique=True, help_text='Código de la promoción, debe ser único.')
    discount    = models.IntegerField(verbose_name='Porcentaje de descuento', help_text='Porcentaje de descuento.')
    service     = models.ForeignKey(Service, on_delete=models.CASCADE, verbose_name='Servicio', help_text='Servicio al cual aplica la promoción', related_name='code_service')
    init_date   = models.DateTimeField(auto_now_add=False, verbose_name='Fecha de inicio', help_text='Fecha de inicio de la promoción')
    end_date    = models.DateTimeField(auto_now_add=False, verbose_name='Fecha de fin', help_text='Fecha para desactivar la promoción')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Código promocional'
        verbose_name_plural ='Códigos promocionales'

class Order(BaseModel):
    service    = models.ForeignKey(Service, on_delete=models.CASCADE, verbose_name='Servicio', help_text='Servicio realizado')
    branch     = models.ForeignKey(Branch, on_delete=models.CASCADE, verbose_name='Sucursal', help_text='Sucursal')
    commissionquantity = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Comisión', help_text='Comisión generada por el servicio')
    commission_dentist = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Comisión por dentista (cantidad)', help_text='Comisión cantidad')
    commission_doctor  = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Comisión por doctor (porcentaje)', help_text='Comisión porcentaje')
    doctor     = models.ForeignKey(Doctor, on_delete=models.CASCADE, verbose_name='Doctor',   help_text='Doctor asignado')
    hprice     = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Precio servicio', help_text='Precio histórico del servicio.')
    total      = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Total', help_text='Importe total de la operación.')
    promo      = models.ForeignKey(Promo, on_delete=models.CASCADE, verbose_name='Promoción', help_text='Promoción aplicada', blank=True, null=True)
    code       = models.ForeignKey(Code, on_delete=models.CASCADE, verbose_name='Código promocional', blank=True, null=True)
    quantity   = models.IntegerField(verbose_name='Cantidad', help_text='Cantidad de servicios solicitados', default=1)
    patient    = models.CharField(max_length=200, verbose_name='Nombre del paciente', help_text='Nombre completo del paciente', blank=True)
    age        = models.IntegerField(verbose_name='Edad del paciente', default=0)
    discount   = models.IntegerField(verbose_name='Porcentaje de descuento por promoción', default=0)
    discountquantity   = models.IntegerField(verbose_name='Cantidad calculada del descuento', default=0)

    def __str__(self):
        return '{0}-{1}'.format(self.service.code, self.pk)

    def name(self):
        return '{0}-{1}'.format(self.service.code, self.pk)

    class Meta:
        verbose_name = 'Orden de Servicio'
        verbose_name_plural  = 'Ordenes de Servicio'

#Events =============================
@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()