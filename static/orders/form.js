//models
function Service(data)
{
	this.id 				= ko.observable(data.id);
	this.code 				= ko.observable(data.code);
	this.commission_dentist = ko.observable(data.commission_dentist);
	this.commission_doctor 	= ko.observable(data.commission_doctor);
	this.created_date 		= ko.observable(data.created_date);
	this.description 		= ko.observable(data.description);
	this.modified_date 		= ko.observable(data.modified_date);
	this.price 				= ko.observable(data.price);
	this.resource_uri 		= ko.observable(data.resource_uri);
}

function Doctor(data)
{
	var self = this;

	this.id 			= ko.observable(data.id);
	this.address 		= ko.observable(data.address);
	this.created_date 	= ko.observable(data.created_date);
	this.email 			= ko.observable(data.email);
	this.hospital 		= ko.observable(data.hospital);
	this.lastname 		= ko.observable(data.lastname);
	this.middlename 	= ko.observable(data.middlename);
	this.name 			= ko.observable(data.name);
	this.phone 			= ko.observable(data.phone);
	this.type_doc 		= ko.observable(data.type_doc);
	this.document 		= ko.observable(data.document);

	this.fullname = ko.computed(()=>
	{
		return self.name() + ' ' + self.middlename() + ' ' + self.lastname();
	});

	this.__str__ = ko.computed(()=>
	{
		return self.fullname() + ', Cédula: ' + self.document() +', ' + self.address() + ', Tel.: ' + self.phone();
	});
}

function Promo(data)
{
	this.created_date 	= ko.observable(data.created_date);
	this.discount 		= ko.observable(data.discount);
	this.end_date 		= ko.observable(data.end_date);
	this.id 			= ko.observable(data.id);
	this.init_date 		= ko.observable(data.init_date);
	this.name 			= ko.observable(data.name);
}

function PromoCode(data)
{

	this.code 			= ko.observable(data.code);
	this.created_date 	= ko.observable(data.created_date);
	this.discount 		= ko.observable(data.discount);
	this.end_date 		= ko.observable(data.end_date);
	this.id 			= ko.observable(data.id);
	this.init_date 		= ko.observable(data.init_date);
	this.modified_date 	= ko.observable(data.modified_date);
	this.name 			= ko.observable(data.name);
	this.resource_uri 	= ko.observable(data.resource_uri);
	this.service 		= ko.observable(data.service);
}

function FormViewModel()
{
	var self = this;
	self.Init();

	//control vars
	self.ispromo     	 = ko.observable(false);
	self.iscodepromo 	 = ko.observable(false);
	self.canvalidatecode = ko.observable(true);

	self.iserror01 = ko.observable(false);
	self.iserror02 = ko.observable(false);
	self.iserror03 = ko.observable(false);
	self.iserror04 = ko.observable(false);

	//properties
	self.Services = ko.observableArray();
	self.Doctors  = ko.observableArray();

	self.SelectedService = ko.observable();
	self.SelectedDoctor  = ko.observable();

	self.Patient	= ko.observable();
	self.Age		= ko.observable();
	self.Quantity   = ko.observable();
	self.PromoCodeInput  = ko.observable();

	self.default_branch = $('#default_branch').val();

	self.Promo = new Promo(
	{
		created_date: undefined,
		discount 	: undefined,
		end_date 	: undefined,
		id 			: undefined,
		init_date 	: undefined,
		name 		: undefined,
	});

	self.PromoCode = new PromoCode(
	{
		code 			: undefined,
		created_date 	: undefined,
		discount 		: undefined,
		end_date 		: undefined,
		id 				: undefined,
		init_date 		: undefined,
		modified_date 	: undefined,
		name 			: undefined,
		resource_uri 	: undefined,
		service 		: undefined,
	});

	self.isage = ko.computed(()=>
	{
		return (self.Age() > 0) ? true : false;
	});

	self.SelectedServiceUI = ko.computed(()=>
	{
		var value = '';

		if(self.SelectedService() > 0)
		{
			var match = ko.utils.arrayFirst(self.Services(), (item)=>
			{
			    return item.id() == self.SelectedService();
			});
		}

		if(match)
		{
			value = ko.toJS(match);
		}

		return value;
	});

	self.ServiceDescription = ko.computed(()=>
	{
		if(self.SelectedService() > 0)
		{
			return self.SelectedServiceUI().description
		}
	});

	self.ServicePrice = ko.computed(()=>
	{
		if(self.SelectedService() > 0)
		{
			return self.SelectedServiceUI().price
		}
	});

	self.isservice = ko.computed(()=>
	{
		return (self.SelectedService() > 0)	? true : false;
	});

	self.GetDoctors(()=>
	{
		self.GetServices(()=>
		{
			self.GetPromo();
		});
	});

	self.DoctorUI = ko.computed(()=>
	{
		var value = '';

		if(self.SelectedDoctor() > 0)
		{
			var match = ko.utils.arrayFirst(self.Doctors(), (item)=>
			{
			    return item.id() == self.SelectedDoctor();
			});
		}

		if(match)
		{
			value = match.__str__();
		}

		return value;
	});

	self.Total = ko.computed(()=>
	{
		var value = self.Quantity() * self.ServicePrice();
		return (isNaN(value)) ? 0 : value;
	});

	self.GreatTotal = ko.computed(()=>
	{

		if(self.ispromo())
		{
			return self.Total() - (self.Total() * (self.Promo.discount() / 100));
		}

		if(self.iscodepromo())
		{
			return self.Total() - (self.Total() * (self.PromoCode.discount() / 100));
		}

		return self.Total();

	});

	self.PromoName = ko.computed(()=>
	{
		return self.Promo.name();
	})
}

FormViewModel.prototype.Init = function()
{
    $(document).ajaxStart($.blockUI({ message: null }));
    $(document).ajaxStop($.unblockUI);
	$('body').bootstrapMaterialDesign();
};

FormViewModel.prototype.GetDoctors = function(Callback)
{
	var self = this;

	$.ajax(
	{
		url : '/api/doctor/',
		method: 'GET'
	}).done((data)=>
	{

		var Items = $.map(data.objects, (Object)=>
		{
			return new Doctor(Object)
		});

		self.Doctors(Items);

		if(typeof(Callback) == 'function')
		{
			Callback();
		}

	}).fail((data)=>
	{
		console.log(data);

	});
};

FormViewModel.prototype.GetServices = function(Callback)
{

 	var self = this;

	$.ajax(
	{
		url : '/api/service/?branches=' + self.default_branch,
		method: 'GET'

	}).done((data)=>
	{
		var Items = $.map(data.objects, (Object)=>
		{
			return new Service(Object)
		});

		self.Services(Items);

		if(typeof(Callback) == 'function')
		{
			Callback();
		}

	}).fail((data)=>
	{
		console.log(data);
	});
};

FormViewModel.prototype.GetPromo = function(Callback)
{
 	var self = this;

	$.ajax(
	{
		url : '/api/promo/',
		method: 'GET'

	}).done((data)=>
	{
		if(data.objects.length > 0 )
		{
			self.Promo.created_date(data.objects[0].created_date);
			self.Promo.discount(data.objects[0].discount);
			self.Promo.end_date(data.objects[0].end_date);
			self.Promo.id(data.objects[0].id);
			self.Promo.init_date(data.objects[0].init_date);
			self.Promo.name(data.objects[0].name);

			self.ispromo(true);
			self.canvalidatecode(false);
		}

	}).fail((data)=>
	{
		console.log(data);
	});
};

FormViewModel.prototype.OpenCode = function()
{
	var self = this;
	$('.modal').modal('show');
};

FormViewModel.prototype.ValidateCode = function()
{
 	var self = this;

	$.ajax(
	{
		url : '/api/code/?code=' + self.PromoCodeInput() + '&service=' + self.SelectedService(),
		method: 'GET'

	}).done((data)=>
	{
		if(data.objects.length > 0 )
		{
			self.PromoCode.code(data.objects[0].code);
			self.PromoCode.created_date(data.objects[0].created_date)
			self.PromoCode.discount(data.objects[0].discount)
			self.PromoCode.end_date(data.objects[0].end_date)
			self.PromoCode.id(data.objects[0].id)
			self.PromoCode.init_date(data.objects[0].init_date)
			self.PromoCode.modified_date(data.objects[0].modified_date)
			self.PromoCode.name(data.objects[0].name)
			self.PromoCode.resource_uri(data.objects[0].resource_uri)
			self.PromoCode.service(data.objects[0].service)

			alert('Código validado.');
			$('.modal').modal('hide');
			self.iscodepromo(true)
			return true;
		}

		alert('El código no es válido.')

	}).fail((data)=>
	{
		console.log(data);
	});
};

FormViewModel.prototype.ChangeBranch = function()
{
	var self  = this;

	var value = $('#userbranch').val();

	$('#default_branch').val(value);
	self.default_branch = value;

	self.GetServices(()=>
	{
		console.log(ko.toJS(self.Services()));
		self.ResetPage();
	});
};

FormViewModel.prototype.ResetPage = function()
{
	var self = this;

	self.SelectedService(undefined);
	self.SelectedDoctor(undefined);

	self.Patient(undefined);
	self.Age(undefined);
	self.Quantity(undefined);
	self.PromoCodeInput(undefined);

	$('#order-resume').fadeOut('fast');
	$('#resume-error').fadeOut('fast');

	self.iserror01(false);
	self.iserror02(false);
	self.iserror03(false);
	self.iserror04(false);

	self.ispromo(false);
	self.iscodepromo(false);

	self.Promo = new Promo(
	{
		created_date: undefined,
		discount 	: undefined,
		end_date 	: undefined,
		id 			: undefined,
		init_date 	: undefined,
		name 		: undefined,
	});

	self.PromoCode = new PromoCode(
	{
		code 			: undefined,
		created_date 	: undefined,
		discount 		: undefined,
		end_date 		: undefined,
		id 				: undefined,
		init_date 		: undefined,
		modified_date 	: undefined,
		name 			: undefined,
		resource_uri 	: undefined,
		service 		: undefined,
	});
};

FormViewModel.prototype.Next = function()
{
	var self   = this;
	var status = true;

	self.iserror01(false);
	self.iserror02(false);
	self.iserror03(false);
	self.iserror04(false);

	if(self.SelectedService()==undefined)
	{
		status = false;
		self.iserror01(true);
	}

	if(self.Quantity()==undefined)
	{
		status = false;
		self.iserror02(true);
	}

	if(self.SelectedDoctor()==undefined)
	{
		status = false;
		self.iserror03(true);
	}

	if(self.Patient==undefined || self.Age()==undefined)
	{
		status = false;
		self.iserror04(true);
	}

	if(!status)
	{
		$('#resume-error').fadeIn('fast');
	}else
	{
		$('#resume-error').fadeOut('fast');
		$('#order-resume').fadeIn('fast');
		$('#next-button').fadeOut('fast');
		$('#cancel-button').fadeIn('fast');
		$('#create-button').fadeIn('fast');

	    $('html,body').animate({
	        scrollTop: $("#order-resume").offset().top
	    }, 'slow');
	}
};

FormViewModel.prototype.Cancel = function()
{
	var self = this;
	self.ResetPage();

	$('#next-button').fadeIn('fast');
	$('#cancel-button').fadeOut('fast');
	$('#create-button').fadeOut('fast');
};

FormViewModel.prototype.Create = function()
{
	var self = this;
	var the_service = ko.utils.arrayFirst(self.Services(), (item)=>
	{
	    return item.id() == self.SelectedService();
	});

	var the_doctor = ko.utils.arrayFirst(self.Doctors(), (item)=>
	{
	    return item.id() == self.SelectedDoctor();
	});


	if(!the_service)
	{
		alert('No hay servicio seleccionado.');
		return false;
	}

	//calculated data
	var ispromoapplied = false;
	var commissionquantity = 0;
	var discount = 0;
	var discountquantity = 0;
	var total = 0;

	if(self.Promo.id() != undefined){ ispromoapplied = true; }
	if(self.PromoCode.id() != undefined){ ispromoapplied = true; }

	if(ispromoapplied)
	{
		if(self.Promo.id() != undefined)
		{
			discount = self.Promo.discount();
			discountquantity = ( self.Total() ) * (discount / 100);
		}

		if(self.PromoCode.id() != undefined)
		{
			discount = self.PromoCode.discount();
			discountquantity = ( self.Total() ) * (discount / 100);
		}

		total = self.Total() - discountquantity;

	}else
	{
		if(the_doctor.type_doc() == 'medico_general')
		{
			commissionquantity = ( self.Total() ) * (the_service.commission_doctor() / 100)
		}

		if(the_doctor.type_doc() == 'dentista')
		{
			commissionquantity = self.Total() - the_service.commission_dentist();
		}

		total =self.Total();
	}

	var data =
	{
		branch   	: '/api/branch/' + self.default_branch + '/',
		doctor 		: '/api/doctor/' + self.SelectedDoctor() + '/',
		promo 		: (self.Promo.id() != undefined) ? '/api/promo/' + self.Promo.id() + '/' :  undefined,
		code 		: (self.PromoCode.id() != undefined) ? '/api/code/' + self.PromoCode.id() + '/' :  undefined,
		service 	: '/api/service/' + self.SelectedService() + '/',
		commission_dentist 	: the_service.commission_dentist(),
		commission_doctor 	: the_service.commission_doctor(),
		hprice 				: the_service.price(),
		quantity 			: self.Quantity(),
		patient 			: self.Patient(),
		age 				: self.Age(),
		discount 			: discount,
		discountquantity 	: discountquantity,
		commissionquantity  : commissionquantity,
		total 				: total,
	}

	console.log(data);

	$.ajax(
	{
		url    : '/api/order/',
		method : 'POST',
		data   : data

	}).done((data)=>
	{
		var id = data.id
		$('#ok').fadeIn('fast');

		self.ResetPage();

		$('#next-button').fadeIn('fast');
		$('#cancel-button').fadeOut('fast');
		$('#create-button').fadeOut('fast');

		window.scrollTo(0, 0);

		setTimeout(()=>
		{
			$('#ok').fadeOut('fast');
		}, 5000)

		window.open('http://192.168.0.2:8000/printorder/' + id)

	}).fail((data)=>
	{
		alert(data.responseText)
	});
};


ko.applyBindings(new FormViewModel(), document.getElementById("bind-container") );