if (!$) {
    $ = django.jQuery;
}

$(document).ready(()=>
{
	var h3 = django.jQuery('#changelist-filter h3')
	var ul = django.jQuery('#changelist-filter ul')

	var jetselect = django.jQuery('.changelist-filter-select-wrapper')

	django.jQuery(h3[h3.length-1]).remove()
	django.jQuery(ul[ul.length-1]).remove()
	django.jQuery(jetselect[0]).remove()
});

