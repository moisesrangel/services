from django.contrib.auth.decorators import login_required, permission_required
from django.http                    import HttpResponse, HttpResponseServerError
from django.shortcuts               import render
from django.template                import loader

from core.models import Profile
from datetime    import datetime

from core.models import Order

import pdfkit

# Create your views here.
@permission_required('core.add_order', login_url='/admin/login/')
@login_required(login_url='/admin/login/')
def index(request):
    template = loader.get_template('orders/index.html')
    date     = datetime.today().strftime('%d-%m-%Y')
    profile  = Profile.objects.get(user=request.user)

    many_branches = False

    if len(profile.branches.all()) == 0 :
        return HttpResponseServerError()

    if len(profile.branches.all()) > 1:
        many_branches = True

    default_branch  = profile.branches.all()[0].id
    BranchName      = profile.branches.all()[0].name
    BranchAddress   = profile.branches.all()[0].address

    context  = {
        'user'              : request.user,
        'branches'          : profile.branches.all(),
        'date'              : date,
        'default_branch'    : default_branch,
        'BranchName'        : BranchName,
        'BranchAddress'     : BranchAddress,
        'many_branches'     : many_branches,
    }
    return HttpResponse(template.render(context, request))

@permission_required('core.add_order', login_url='/admin/login/')
@login_required(login_url='/admin/login/')
def printorder(request, orderid):
    template = loader.get_template('orders/print.html')
    order    = Order.objects.get(id=orderid)
    context  = { 'order' : order }
    return HttpResponse(template.render(context, request))

@permission_required('core.add_order', login_url='/admin/login/')
@login_required(login_url='/admin/login/')
def getpdf(request, orderid):
    print(orderid)
    pdf = pdfkit.from_url('http://192.168.0.2:8000/printorder/{0}'.format(orderid), False)
    response = HttpResponse(pdf,content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="order_{0}.pdf"'.format(orderid)
    return response

#5587914341