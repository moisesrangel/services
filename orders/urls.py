from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('printorder/<int:orderid>', views.printorder, name='printorder'),
    path('print/<int:orderid>', views.getpdf, name='getpdf'),
]