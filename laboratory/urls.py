"""laboratory URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf 	  import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

from django.contrib   import admin
from django.urls      import path

from api.resources import DoctorResource, ServiceResource, PromoResource, CodeResource, BranchResource, OrderResource

doctor_resource     = DoctorResource()
service_resource    = ServiceResource()
promo_resource      = PromoResource()
code_resource       = CodeResource()
branch_resource     = BranchResource()
order_resource      = OrderResource()


urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),
	url(r'', include('orders.urls')),
    url(r'^api/', include(doctor_resource.urls)),
    url(r'^api/', include(service_resource.urls)),
    url(r'^api/', include(promo_resource.urls)),
    url(r'^api/', include(code_resource.urls)),
    url(r'^api/', include(branch_resource.urls)),
    url(r'^api/', include(order_resource.urls)),
    path('orders/', include('orders.urls')),
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
