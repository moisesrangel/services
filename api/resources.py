from core.models            import Doctor, Service, Promo, Code, Branch, Order
from datetime               import datetime
from tastypie               import fields
from tastypie.authorization import Authorization
from tastypie.resources     import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.serializers   import Serializer
import urllib.parse as urlparse

#https://stackoverflow.com/questions/14074149/tastypie-with-application-x-www-form-urlencoded
class urlencodeSerializer(Serializer):
    formats = ['json', 'jsonp', 'xml', 'yaml', 'html', 'plist', 'urlencode']
    content_types = {
        'json': 'application/json',
        'jsonp': 'text/javascript',
        'xml': 'application/xml',
        'yaml': 'text/yaml',
        'html': 'text/html',
        'plist': 'application/x-plist',
        'urlencode': 'application/x-www-form-urlencoded',
        }

    def from_urlencode(self, data, options=None):
        """ handles basic formencoded url posts """
        qs = {}
        print(data)
        for k, v in urlparse.parse_qs(data).items():
            value = v if len(v)>1 else v[0]
            value = value.encode("latin-1").decode('utf-8')
            qs[k] = value
        return qs

    def to_urlencode(self,content):
        pass

class DoctorResource(ModelResource):
    class Meta:
        queryset = Doctor.objects.all()
        resource_name = 'doctor'

class BranchResource(ModelResource):
    class Meta:
        queryset = Branch.objects.all()
        resource_name = 'branch'

class ServiceResource(ModelResource):
    branches = fields.ToManyField('api.resources.BranchResource', 'branches', null=True)
    class Meta:
        queryset = Service.objects.all()
        resource_name = 'service'
        filtering = { 'branches' : ALL_WITH_RELATIONS }

class PromoResource(ModelResource):
    class Meta:
        now = datetime.today()
        queryset = Promo.objects.filter(init_date__lte=now, end_date__gte=now)
        resource_name = 'promo'

class CodeResource(ModelResource):
    service = fields.ToOneField(ServiceResource, 'service')
    class Meta:
        now = datetime.today()
        queryset = Code.objects.filter(init_date__lte=now, end_date__gte=now)
        resource_name = 'code'
        filtering = { 'code' : ALL, 'service' : ALL_WITH_RELATIONS }

class OrderResource(ModelResource):
    branch  = fields.ForeignKey(BranchResource,  'branch')
    doctor  = fields.ForeignKey(DoctorResource,  'doctor')
    service = fields.ForeignKey(ServiceResource, 'service')
    code    = fields.ForeignKey(CodeResource, 'code', null=True)
    promo   = fields.ForeignKey(PromoResource, 'promo', null=True)
    class Meta:
        always_return_data = True
        serializer    = urlencodeSerializer()
        queryset      = Order.objects.all()
        resource_name = 'order'
        authorization = Authorization() #DEV
