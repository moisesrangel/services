sudo apt-get install wkhtmltopdf

#app settings
MEDIA_ROOT = '/home/moises/laboratory/media/'
MEDIA_URL = '/media/'


#python packs
pip3 install Pillow
#pip3 install django-grappelli
pip install django-suit==0.2.25
pip install django-admin-rangefilter
pip install django-material

pip install django-jet
 python3 manage.py migrate jet
#Database Configuration

sudo -u postgres psql
create database laboratory;
create user laboratory with encrypted password 'Laboratory123$';
grant all privileges on database laboratory to laboratory;